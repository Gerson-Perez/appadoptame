package com.example.gerson.appadoptame.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by gerson on 25/01/18.
 */

public class SQLiteAdoptame extends SQLiteOpenHelper {

    public SQLiteAdoptame(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL("create table usuarios (" +
                "id_user int primary key,"+
                "name_user text,"+
                "last_name text,"+
                "email text,"+
                "password text,"+
                "phone int,"+
                "birthdate real,"+
                "img_user none,"+
                "created_at real,"+
                "updated_at real"+
                " )");

        database.execSQL("create table categoriamascotas (" +
                "id_categori int primary key,"+
                "name_categori text,"+
                "description_categori text,"+
                "created_at real,"+
                "updated_at real"+
                " )");

        database.execSQL("create table mascotas("+
                        "id_mascota int primary key,"+
                        "id_user int,"+
                        "id_categori int,"+
                        "name_mascota text,"+
                        "age text,"+
                        "description_mascota text,"+
                        "img_mascota none,"+
                        "created_at real,"+
                        "updated_at real,"+
                        "foreign key(id_user) references usuarios(id_user),"+
                        "foreign key(id_categori) references categoriamascotas(id_categori)"+
                ")");
    }


    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("drop table if exists usuarios");
        database.execSQL("create table usuarios (" +
                "id_user int primary key,"+
                "name_user text,"+
                "last_name text,"+
                "email text,"+
                "password text,"+
                "phone int,"+
                "birthdate real,"+
                "img_user none,"+
                "created_at real,"+
                "updated_at real"+
                " )");

        database.execSQL("drop table if exists categoriamascotas");
        database.execSQL("create table categoriamascotas (" +
                "id_categori int primary key,"+
                "name_categori text,"+
                "description_categori text,"+
                "created_at real,"+
                "updated_at real"+
                " )");
        database.execSQL("drop table if exists mascotas");
        database.execSQL("create table mascotas("+
                "id_mascota int primary key,"+
                "id_user int,"+
                "id_categori int,"+
                "name_mascota text,"+
                "age text,"+
                "description_mascota text,"+
                "img_mascota none,"+
                "created_at real,"+
                "updated_at real,"+
                "foreign key(id_user) references usuarios(id_user),"+
                "foreign key(id_categori) references categoriamascotas(id_categori)"+
                ")");
    }


}
