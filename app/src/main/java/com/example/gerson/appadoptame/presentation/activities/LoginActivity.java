package com.example.gerson.appadoptame.presentation.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.gerson.appadoptame.BuildConfig;
import com.example.gerson.appadoptame.R;
import com.example.gerson.appadoptame.data.database.SQLiteAdoptame;
import com.example.gerson.appadoptame.presentation.helpers.PreferencesHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText email;
    EditText password;
    TextView forgotPass;
    TextView signup;
    AppCompatButton btnLogin;
    ProgressBar progressBarLogin;

    public RequestQueue requestQueue;
    public StringRequest stringRequest;
    public String url;
    public String token;
    public SQLiteAdoptame admin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (EditText)findViewById(R.id.txtEmail);
        password = (EditText)findViewById(R.id.txtPassword);
        forgotPass = (TextView) findViewById(R.id.lbl_recoverPassword);
        signup = (TextView) findViewById(R.id.lbl_signup);
        btnLogin = (AppCompatButton) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validarMensajero();
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotPass();
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerCurrier();
            }
        });


    }

    public void showProgress() {
        progressBarLogin.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBarLogin.setVisibility(View.GONE);
    }

    public void setErrorEmail() {
        email.setError("");
    }

    public void setErrorPassword() {
        password.setError("");
    }

    public void loginSuccess() {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void loginError() {
        Toast.makeText(LoginActivity.this, "Credenciales incorectas", Toast.LENGTH_LONG).show();
    }

    public void registerCurrier(){
        //startActivity(new Intent(this, RegisterActivity.class));
    }

    public void forgotPass(){
        //startActivity(new Intent(this, ForgotPassActivity.class));
    }


    public void validarMensajero() {

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                //Hacer logica para poder hacer la peticion al servidor

                if (!email.getText().toString().equals("")  && !password.getText().toString().equals("")) {

                    //token = FirebaseInstanceId.getInstance().getToken();
                    admin = new SQLiteAdoptame(getApplication(), "administracion", null, 1);

                    requestQueue = Volley.newRequestQueue(getApplicationContext());
                    url =  "http://34.232.70.43:8080" + "/login.php";
                    Log.d("url",url.toString());
                    stringRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("body", response.toString());
                                    try {
                                        JSONObject o = new JSONObject(response);
                                        String code = o.getString("status");

                                        if (code.equals("successful")) {
                                            String data = o.getString("mesagge");
                                            PreferencesHelper.saveSession(getApplicationContext(), email.getText().toString(), password.getText().toString(), "HOla Soy Un TOKEN");

                                            JSONObject objectCurrier = new JSONObject(data);
                                            SQLiteDatabase bd = admin.getWritableDatabase();
                                            ContentValues registro = new ContentValues();
                                            registro.put("idClient", objectCurrier.getInt("idClient"));
                                            registro.put("email", objectCurrier.getString("email"));
                                            registro.put("password", objectCurrier.getString("password"));
                                            registro.put("name", objectCurrier.getString("name"));
                                            registro.put("lastName", objectCurrier.getString("lastName"));
                                            registro.put("birthday", (objectCurrier.getString("birthday") == null  ? "" : objectCurrier.getString("birthday")) );
                                            registro.put("password", objectCurrier.getString("password"));
                                            bd.insert("user", null, registro);
                                            bd.close();

                                            loginSuccess();
                                        } else if(code.equals("error")){
                                            loginError();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> parametros = new HashMap<>();
                            parametros.put("email", email.getText().toString());
                            parametros.put("password", password.getText().toString());
                            return parametros;
                        }
                    };
                    requestQueue.add(stringRequest);
                } else {
                    if (email.getText().toString().equals(""))
                        setErrorEmail();
                    if (password.getText().toString().equals(""))
                        setErrorPassword();
                }


            }
        }, 500 );

    }

}