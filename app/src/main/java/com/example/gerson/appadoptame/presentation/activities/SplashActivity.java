package com.example.gerson.appadoptame.presentation.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.gerson.appadoptame.R;
import com.example.gerson.appadoptame.presentation.helpers.PreferencesHelper;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        onStartValidate(PreferencesHelper.isSignedIn(getApplication()));
    }

    public void onSuccesSession() {
        startActivity(new Intent(this, MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setAction(Intent.ACTION_MAIN));
        finish();

    }

    public void onErrorSession() {
        startActivity(new Intent(this, LoginActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .setAction(Intent.ACTION_MAIN));
        finish();
    }

    public void onStartValidate(final boolean session) {
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                if(session)
                     onSuccesSession();
                else
                     onErrorSession();
            }
        }, 175);
    }


}
