package com.example.gerson.appadoptame.presentation.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by gerson on 25/01/18.
 */

public class PreferencesHelper {

    private static final String DOMAIN_PREFERENCES = "APPADOPTAME";
    private static final String PREFERENCES_EMAIL = DOMAIN_PREFERENCES + ".EMAIL";
    private static final String PREFERENCES_PASSWORD = DOMAIN_PREFERENCES + ".PASSWORD";

    public static void signOut(Context context) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.remove(PREFERENCES_EMAIL);
        editor.remove(PREFERENCES_PASSWORD);
        editor.apply();
    }

    public static void saveSession(Context context, String username, String password){
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(PREFERENCES_EMAIL, username);
        editor.putString(PREFERENCES_PASSWORD, password); 
        editor.apply();
    }

    public static final String getUserSession(Context context){
        SharedPreferences sharedPreferences= getSharedPreferences(context);
        String username= sharedPreferences.getString(PREFERENCES_EMAIL,null);
        return username;
    }

    public static boolean isSignedIn(Context context) {
        final SharedPreferences preferences = getSharedPreferences(context);
        return preferences.contains(PREFERENCES_EMAIL) &&
                preferences.contains(PREFERENCES_PASSWORD) ;
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.edit();
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES_EMAIL, Context.MODE_PRIVATE);
    }
}
