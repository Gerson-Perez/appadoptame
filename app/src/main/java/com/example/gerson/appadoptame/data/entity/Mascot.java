package com.example.gerson.appadoptame.data.entity;

import android.graphics.Bitmap;

import java.security.PrivateKey;

/**
 * Created by gerson on 25/01/18.
 */

public class Mascot {
    private int idUser;
    private int idCategori;
    private String nameMascota;
    private int ageMascota;
    private String descriptionMascota;
    private Bitmap imgMascota;
}
